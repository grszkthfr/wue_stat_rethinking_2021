# Statistical Rethinking: A Bayesian Grass Root Course :sunflower:

March/April 2021

Instructor: we

Format: Online, flipped instruction. The lectures are pre-recorded. We'll meet
online twice a day for an hour to see and discuss the lectures, and work through
the solutions to the assigned problems.

When: Every morning 9:00h - 10:00h we discuss the lectures and from 15:00 -
16:00h we discuss the assignments. A Zoom link and a dedicated slack channel
will be prepared.

Registration: none

## Book

We'll use the 2nd edition of Richard McElreaths book, Statistical Rethinking.
BYO or find an online copy from the University Library Würzburg
[here](https://ebookcentral.proquest.com/lib/ub-wuerzburg/detail.action?docID=6133700).

## Lectures

The full lecture video playlist is here: <YouTube:[Statistical Rethinking
2019](https://www.youtube.com/playlist?list=PLDcUM9US4XdNM4Edgs7weiyIguLSToZRI)>.
Links to individual lectures, slides and videos are in the calendar at the very
bottom.

## Code examples

Students can engage with the material using either the original R code examples
or one of several conversions to other computing environments. The conversions
are not always exact, but they are rather complete.

### Original R Flavor

For those who want to use the original R code examples in the print book, you
need to first install `rstan`. Go to <http://mc-stan.org/> and find the
instructions for your platform. **Then** you can install the `rethinking`
package:

```
install.packages(c("devtools","mvtnorm","loo","coda"),dependencies=TRUE)
library(devtools)
install_github("rmcelreath/rethinking")
```

The code is all on github <https://github.com/rmcelreath/rethinking/> and there
are additional details about the package there, including information about
using the more-up-to-date `cmdstanr` instead of `rstan` as the underlying MCMC
engine.

### Conversions

Feel free to use the language and framework of your choice. See the full list at
<https://xcelab.net/rm/statistical-rethinking/>.

## Homework and solutions

tbd

## Calendar & Topical Outline

| Day # - Date     | Session        | Reading         | Lectures                                                                                                                                                                                                                                                                                                                                                                                 |
| -------          | -------------- | --------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------                                                                                           |
|                  | homework       | Chapters  1 - 3 | The Golem of Prague <[slides](https://speakerdeck.com/rmcelreath/l01-statistical-rethinking-winter-2019)> <[video](https://www.youtube.com/watch?v=4WVelCswXo4)> <br>Garden of Forking Data <[slides](https://speakerdeck.com/rmcelreath/l02-statistical-rethinking-winter-2019)> <[video](https://www.youtube.com/watch?v=XoVtOAN0htU&list=PLDcUM9US4XdNM4Edgs7weiyIguLSToZRI&index=2)> |
| Day 1 - 29 March | morning        | Chapter 4       | Geocentric Models <[slides](https://speakerdeck.com/rmcelreath/l03-statistical-rethinking-winter-2019)> <[video](https://youtu.be/h5aPo5wXN8E)>                                                                                                                                                                                                                                          |
|                  | afternoon      | Chapter 4       | Wiggly Orbits <[slides](https://speakerdeck.com/rmcelreath/l04-statistical-rethinking-winter-2019)> <[video](https://youtu.be/ENxTrFf9a7c)>                                                                                                                                                                                                                                              |
| Day 2 - 30 March | morning        | Chapter 5       | Spurious Waffles <[slides](https://speakerdeck.com/rmcelreath/l05-statistical-rethinking-winter-2019)> <[video](https://www.youtube.com/watch?v=e0tO64mtYMU&index=5&list=PLDcUM9US4XdNM4Edgs7weiyIguLSToZRI)>                                                                                                                                                                            |
|                  | afternoon      | Chapter 6       | Haunted DAG <[slides](https://speakerdeck.com/rmcelreath/l06-statistical-rethinking-winter-2019)> <[video](https://youtu.be/l_7yIUqWBmE)>                                                                                                                                                                                                                                                |
| Day 3 - 31 March | morning        | Chapter 7       | Ulysses' Compass <[slides](https://speakerdeck.com/rmcelreath/l07-statistical-rethinking-winter-2019)> <[video](https://youtu.be/0Jc6Kgw5qc0)>                                                                                                                                                                                                                                           |
|                  | afternoon      | Chapter 7       | Model Comparison <[slides](https://speakerdeck.com/rmcelreath/l08-statistical-rethinking-winter-2019)> <[video](https://youtu.be/gjrsYDJbRh0)>                                                                                                                                                                                                                                           |
| Day 4 - 1 April  | morning        | Chapter 8       | Conditional Manatees <[slides](https://speakerdeck.com/rmcelreath/l09-statistical-rethinking-winter-2019)> <[video](https://youtu.be/QhHfo6-Bx8o)>                                                                                                                                                                                                                                       |
|                  | afternoon      | Chapter 9       | Markov Chain Monte Carlo <[slides](https://speakerdeck.com/rmcelreath/l10-statistical-rethinking-winter-2019)> <[video](https://youtu.be/v-j0UmWf3Us)>                                                                                                                                                                                                                                   |
| Day 5 - 2 April  | morning        | Chapter 10      | Maximum entropy & GLMs <[slides](https://speakerdeck.com/rmcelreath/l11-statistical-rethinking-winter-2019)> <[video](https://youtu.be/-4y4X8ELcEM)>                                                                                                                                                                                                                                     |
|                  | afternoon      | Chapter 11      | God Spiked the Integers <[slides](https://speakerdeck.com/rmcelreath/l12-statistical-rethinking-winter-2019)> <[video](https://youtu.be/hRJtKCIDTwc)>                                                                                                                                                                                                                                    |
|                  |                |                 |                                                                                                                                                                                                                                                                                                                                                                                          |
|                  |                | Chapter 12      | Monsters & Mixtures <[slides](https://speakerdeck.com/rmcelreath/l13-statistical-rethinking-winter-2019)> <[video](https://youtu.be/p7g-CgGCS34)> <br>Ordered Categories, Left & Right <[slides](https://speakerdeck.com/rmcelreath/l14-statistical-rethinking-winter-2019)> <[video](https://youtu.be/zA3Jxv8LOrA)>                                                                     |
|                  |                | Chapter 13      | Multilevel Models <[slides](https://speakerdeck.com/rmcelreath/l15-statistical-rethinking-winter-2019)> <[video](https://youtu.be/AALYPv5xSos)> <br>Multilevel Models 2 <[slides](https://speakerdeck.com/rmcelreath/l16-statistical-rethinking-winter-2019)> <[video](https://youtu.be/ZG3Oe35R5sY)>                                                                                    |
|                  |                | Chapter 14      | Adventures in Covariance <[slides](https://speakerdeck.com/rmcelreath/l17-statistical-rethinking-winter-2019)> <[video](https://youtu.be/yfXpjmWgyXU)> <br> Slopes, Instruments and Social Relations <[slides](https://speakerdeck.com/rmcelreath/l18-statistical-rethinking-winter-2019)> <[video](https://youtu.be/e5cgiAGBKzI)>                                                       |
|                  |                | Chapter 15      | Gaussian Processes <[slides](https://speakerdeck.com/rmcelreath/l19-statistical-rethinking-winter-2019)> <[video](https://youtu.be/pwMRbt2CbSU)>  <br>Missing Values and Measurement Error <[slides](https://speakerdeck.com/rmcelreath/l20-statistical-rethinking-winter-2019)> <[video](https://youtu.be/UgLF0aLk85s)>                                                                 |



